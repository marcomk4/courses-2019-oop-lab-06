package it.unibo.oop.lab06.generics1;

import java.util.List;
import java.util.Set;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;

public class GraphImpl<N> implements Graph<N> {
	
	final Map<N, Set<N>> linkMap = new LinkedHashMap<>();
	
	public GraphImpl () {
		
	}
	
	public void addNode(N node) {
		if (!linkMap.containsKey(node)) {
			linkMap.put(node, new HashSet<N>());
		}
		
	}
	
	public void addEdge(N source, N target) {
		if ((linkMap.containsKey(source)) && (linkMap.containsKey(target))) {
			linkMap.get(source).add(target);
		}
		else {
			throw new IllegalArgumentException("No such node: ");
		}
	}
	
	public Set<N> nodeSet() {
		return new HashSet<>(linkMap.keySet());
	}
	
	public Set<N> linkedNodes(N node) {
		return linkMap.get(node);
	}
	
	private int getNodesCount() {
        return linkMap.keySet().size();
    }
	
	public List<N> getPath(N source, N target) {
		
		if ((linkMap.containsKey(source)) && (linkMap.containsKey(target))) {
			final Deque<Step<N>> fringe = new LinkedList<>();
	        fringe.add(new Step<>(source));
	        final Set<N> alreadyVisited = new HashSet<>();
	        while (!fringe.isEmpty() && alreadyVisited.size() < getNodesCount()) {
	            final Step<N> lastStep = fringe.poll();
	            final N currentNode = lastStep.getPosition();

	            if (currentNode.equals(target)) {
	                return lastStep.getPath();
	            } else if (!alreadyVisited.contains(currentNode)) {
	                alreadyVisited.add(currentNode);
	                updateFringe(fringe, lastStep);
	            }
	        }
	        return Collections.emptyList();
			
		}
		else {
			return Collections.emptyList();
		}
	}
	
	private void updateFringe(final Deque<Step<N>> fringe, final Step<N> lastStep) {
        final N currentNode = lastStep.getPosition();
        for (final N reachableNode : linkedNodes(currentNode)) {
            fringe.addLast(new Step<>(lastStep, reachableNode));
        }
    }
}
