package it.unibo.oop.lab.exception2;

import org.junit.Test;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	
    	final AccountHolder marco = new AccountHolder ("Marco", "Rossi", 1);
    	final AccountHolder romina = new AccountHolder ("Romina", "Parini", 2);
    	
    	final StrictBankAccount marcoAcc = new StrictBankAccount (marco.getUserID(), 10000, 10);
    	final StrictBankAccount rominaAcc = new StrictBankAccount (romina.getUserID(), 10000, 10);
    	
    	try {
    		marcoAcc.withdraw(2, 1000);
    		fail();
    	} catch (WrongAccountHolderException e) {
    		assertNotNull(e);
    	}
    	
    	try {
    		marcoAcc.withdraw(marco.getUserID(), 11000);
    		fail();
    	} catch (NotEnoughFoundsException e) {
    		assertNotNull(e);
    	}
    	
    	try {
    		rominaAcc.withdrawFromATM(1, 1000);
    		fail();
    	} catch (WrongAccountHolderException e) {
    		assertNotNull(e);
    	}
    	try {
    		for (int i = 0; i < 10; i++) {
    			marcoAcc.deposit(marco.getUserID(), 1);
    		}
    	} catch (TransactionsOverQuotaException e) {
    		assertNotNull(e);
    	}
    	
    	try {
    		marcoAcc.withdraw(marco.getUserID(), 10009);
    	} catch (NotEnoughFoundsException e) {
    		assertNotNull(e);
    	}
    	
    	try {
    		marcoAcc.computeManagementFees(2);
    		fail();
    	} catch (WrongAccountHolderException e) {
    		assertNotNull(e);
    	}
    	
    	try {
    		marcoAcc.computeManagementFees(1);
    		fail();
    	} catch (NotEnoughFoundsException e) {
    		assertNotNull(e);
    	}
    }
}
