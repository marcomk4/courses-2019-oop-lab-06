package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends IllegalStateException{
	
	public String toString() {
		return "Transactions over quota";
	}
}
