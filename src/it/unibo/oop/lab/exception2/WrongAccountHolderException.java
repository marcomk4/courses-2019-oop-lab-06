package it.unibo.oop.lab.exception2;

/**
 * 
 */
public class WrongAccountHolderException extends IllegalArgumentException {

    public String toString() {
    	return "Wrong account holder";
    }

}
