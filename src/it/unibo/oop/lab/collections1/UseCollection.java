package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	private static final int TO_MS = 1_000_000;
    private static final int ELEMS = 100_000;
    private static final int READS = 10_000;
    private static final int START = 1000;
    private static final int END = 2000;
    private static final String NS = "ns (";
    private static final String MS = "ms).";

    private static final long AFRICA_POPULATION = 1_110_635_000L;
    private static final long AMERICAS_POPULATION = 972_005_000L;
    private static final long ANTARTICA_POPULATION = 0L;
    private static final long ASIA_POPULATION = 4_298_723_000L;
    private static final long EUROPE_POPULATION = 742_452_000L;
    private static final long OCEANIA_POPULATION = 38_304_000L;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void measureAdd (List<Integer> list, String x) {
    	long time = System.nanoTime();
    	for (int i = 1; i <= ELEMS; i++) {
            list.add(0, i);
        }
    	
    	time = System.nanoTime() - time;
        System.out.println("Add " + ELEMS 
                + " elements in a " + x + " took " + time
                + "ns (" + time / TO_MS + "ms)");
    }
    
    public static void measureGet (List<Integer> list, String x) {
    	long time = System.nanoTime();
        for (int d = 1; d <= READS; d++) {
        	list.get(list.size() / 2);
        }
        
        time = System.nanoTime() - time;
        System.out.println("Read " + READS
                + " elements in half position of a " + x + " took " + time
                + "ns (" + time / TO_MS + "ms)");
    }
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	final List<Integer> numberList = new ArrayList<>();
    	for (int i = START; i < END; i++) {
    		numberList.add(i);
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	final LinkedList<Integer> numbLinkList = new LinkedList<>(numberList);
    	System.out.println(numbLinkList);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	final int nvariable = numberList.set(0, numberList.get(numberList.size()-1));
    	numberList.set(numberList.size()-1, nvariable);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (final int ele : numberList) {
    		System.out.print(ele);
    		System.out.print(", ");
    	}
    	System.out.println();
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	measureAdd(numberList, "ArrayList");
    	measureAdd(numbLinkList, "LinkedList");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	measureGet(numberList, "ArrayList");
    	measureGet(numbLinkList, "LinkedList");
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        
        Map<String,Long> worldMap = new HashMap<>();
        worldMap.put("Africa", AFRICA_POPULATION);
        worldMap.put("Americas", AMERICAS_POPULATION);
        worldMap.put("Antarctica", ANTARTICA_POPULATION);
        worldMap.put("Asia", ASIA_POPULATION);
        worldMap.put("Europe", EUROPE_POPULATION);
        worldMap.put("Oceania", OCEANIA_POPULATION);
        
        /*
         * 8) Compute the population of the world
         */
        long sum = 0;
        for (final long elem : worldMap.values()) {
        	sum += elem;
        }
        worldMap.put("World", sum);
        System.out.println(worldMap);
    }
}
